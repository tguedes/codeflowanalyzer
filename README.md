# CodeFlowAnalyzer

## Running

1. Clone the project
2. Run "dotnet build"
3. Run "dotnet run --project CodeFlowAnalyzer"
4. Open the file that is output to the console to verify

## TODO
1. Update to use mermaid
2. Only works with If statements right now.

All the methods which need to be overridden:
https://docs.microsoft.com/en-us/dotnet/api/microsoft.codeanalysis.csharp.csharpsyntaxwalker?view=roslyn-dotnet