using System.Text;
using CodeFlowAnalyzer.Nodes;

namespace CodeFlowAnalyzer.GraphString
{
    public class GraphStringBuilder
    {
        private readonly StringBuilder _stringBuilder;

        public GraphStringBuilder()
        {
            _stringBuilder = new StringBuilder();
            Initialize("MyGraph");
        }

        public GraphStringBuilder(string graphName)
        {
            _stringBuilder = new StringBuilder();
            Initialize(graphName);
        }
        
        private void Initialize(string graphName)
        {
            _stringBuilder.AppendLine($"digraph {graphName} {{");
        }

        public GraphStringBuilder AddNodeDeclaration(Node n)
        {
            _stringBuilder.AppendLine($"${n.GetId()} [shape={n.GetShape().ToString()}, label=\"{n.GetDisplayText()}\"]");
            return this;
        }

        public GraphStringBuilder AddNodeLink(int from, int to)
        {
            _stringBuilder.AppendLine($"{from} -> {to} [color=black]");
            return this;
        }

        public GraphStringBuilder EndGraph()
        {
            _stringBuilder.AppendLine("}");
            return this;
        }
    }
}