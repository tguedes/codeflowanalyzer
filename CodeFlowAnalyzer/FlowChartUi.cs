using System;
using System.Collections.Generic;
using System.Diagnostics;
using CodeFlowAnalyzer.Nodes;
using CodeFlowAnalyzer.GraphString;

namespace CodeFlowAnalyzer
{
    public class FlowChartUi
    {
        private readonly GraphStringBuilder _graphStringBuilder;
        private readonly Node _currentNode;
        private readonly IList<Node> _allNodes;
        public FlowChartUi(GlobalNode node, IList<Node> allNodes)
        {
            _graphStringBuilder = new GraphStringBuilder();
            _currentNode = node;
            _allNodes = allNodes;
        }

        public void Process()
        {
            ConstructGraphString();
        }

        private void ConstructGraphString()
        {
            // Declare Nodes
            foreach (Node n in _allNodes)
            {
                _graphStringBuilder.AddNodeDeclaration(n);
            }
            
            PrintGlobalNode();
        }

        private void PrintGlobalNode()
        {
            var globalNode = _currentNode as GlobalNode;
            var children = globalNode.GetChildren();
            
            for (var i = children.Count - 1; i > 0; i--)
            {
                PrintLink(children[i - 1], children[i]);
            }
            
            
            PrintLink(globalNode, children[0]);
        }

        private void PrintLink(Node previousNode, Node currentNode)
        {
            switch (previousNode.GetNodeType())
            {
                case "global":
                    ConstructLink(previousNode, currentNode);
                    break;
                case "if":
                    PrintIfLink(previousNode as IfNode, currentNode);
                    break;
                case "statement":
                    ConstructLink(previousNode, currentNode);
                    break;
                default:
                    throw new Exception("Invalid node type: " + previousNode.GetNodeType());
            }
        }

        private void PrintIfLink(IfNode previousNode, Node currentNode)
        {
            var ifBranchNodes = previousNode.GetIfBranchNodes();
            if (ifBranchNodes.Count == 0)
            {
                ConstructLink(previousNode, currentNode);
            }
            else
            {
                var lastIndex = ifBranchNodes.Count - 1;
                PrintLink(ifBranchNodes[lastIndex], currentNode);
                for (var i = lastIndex; i > 0; i--)
                {
                    PrintLink(ifBranchNodes[i - 1], ifBranchNodes[i]);
                }
                ConstructLink(previousNode, ifBranchNodes[0]);
            }

            var elseBranchNodes = previousNode.GetElseBranchNodes();
            if (elseBranchNodes.Count == 0)
            {
                ConstructLink(previousNode, currentNode);
            }
            else
            {
                var lastIndex = elseBranchNodes.Count - 1;
                PrintLink(elseBranchNodes[lastIndex], currentNode);
                for (var i = lastIndex; i > 0; i--)
                {
                    PrintLink(elseBranchNodes[i - 1], elseBranchNodes[i]);
                }
                ConstructLink(previousNode, elseBranchNodes[0]);
            }
        }
        
        private void ConstructLink(Node from, Node to)
        {
            _graphStringBuilder.AddNodeLink(from.GetId(), to.GetId());
        }
         
         public void OutputGraphToFile(string fileBaseName)
         {
             _graphStringBuilder.EndGraph();

             var fileName = fileBaseName + ".dot";
             
             var filePath = AppDomain.CurrentDomain.BaseDirectory + fileName;
             System.IO.File.WriteAllText(filePath, _graphStringBuilder.ToString());

             string cmd = $"dot {filePath} -Tpng -O";
             var process = new Process()
             {
                 StartInfo = new ProcessStartInfo
                 {
                     FileName = "/bin/bash",
                     Arguments = $"-c \"{cmd}\"",
                     RedirectStandardOutput = true,
                     UseShellExecute = false,
                     CreateNoWindow = true,
                 }
             };
             process.Start();
             process.StandardOutput.ReadToEnd();
             process.WaitForExit();
         }
    }
}