namespace CodeFlowAnalyzer.Nodes
{
    public interface IScope
    {
        void Add(Node newNode);
    }
}