using CodeFlowAnalyzer.GraphString;

namespace CodeFlowAnalyzer.Nodes
{
    public class StatementNode : Node
    {
        public StatementNode(string displayText, int id, int scope) :
            base(displayText, id, scope)
        { }
        
        public StatementNode(string displayText, int id) :
            base(displayText, id)
        { }

        public override Shape GetShape()
        {
            return Shape.Box;
        }

        public override string GetNodeType()
        {
            return "statement";
        }

        public override int GetNumberOfChildren()
        {
            return 1;
        }
    }
}