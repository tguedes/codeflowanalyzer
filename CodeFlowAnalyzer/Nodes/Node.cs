using CodeFlowAnalyzer.GraphString;

namespace CodeFlowAnalyzer.Nodes
{
    public abstract class Node
    {
        private readonly int _id;

        private readonly string _displayText;

        private readonly int _scope;
        
        protected Node(string displayText, int id)
        {
            _displayText = displayText;
            _id = id;
        }
                
        protected Node(string displayText, int id, int scope)
        {
            _displayText = displayText;
            _id = id;
            _scope = scope;
        }

        public int GetId()
        {
            return _id;
        }

        public string GetDisplayText()
        {
            return _displayText;
        }

        public int GetScope()
        {
            return _scope;
        }

        //make enum
        public abstract Shape GetShape();

        public abstract int GetNumberOfChildren();

        public abstract string GetNodeType();

        public override int GetHashCode()
        {
            return _id;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Node);
        }

        private bool Equals(Node node)
        {
            return _id == node._id;
        }

        public override string ToString()
        {
            return _displayText;
        }
    }
}