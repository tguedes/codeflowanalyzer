using System.Collections.Generic;
using CodeFlowAnalyzer.GraphString;

namespace CodeFlowAnalyzer.Nodes
{
    public class GlobalNode : Node, IScope
    {
        private readonly IList<Node> _nodes;
        
        public GlobalNode(string displayText, int id, int scope) :
            base(displayText, id, scope)
        {
            _nodes = new List<Node>();
        }

        public GlobalNode(string displayText, int id)
            : base(displayText, id)
        {
            _nodes = new List<Node>();
        }

        public override string GetNodeType()
        {
            return "global";
        }

        public override Shape GetShape()
        {
            return Shape.Box;
        }

        public override int GetNumberOfChildren()
        {
            return 0;
        }

        public IList<Node> GetChildren()
        {
            return _nodes;
        }

        public void Add(Node newNode)
        {
            _nodes.Add(newNode);
        }
    }
}