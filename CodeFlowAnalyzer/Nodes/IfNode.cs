using System.Collections.Generic;
using CodeFlowAnalyzer.GraphString;

namespace CodeFlowAnalyzer.Nodes
{
    public class IfNode : Node, IScope
    {
        private readonly IList<Node> _ifBranch;

        private readonly IList<Node> _elseBranch;

        private bool _ifBranchDone;
        
        public IfNode(string displayText, int id, int scope) :
            base(displayText, id, scope)
        {
            _ifBranch = new List<Node>();
            _elseBranch = new List<Node>();
        }
        
        public IfNode(string displayText, int id)
            : base(displayText, id)
        {
            _ifBranch = new List<Node>();
            _elseBranch = new List<Node>();
        }

        public IList<Node> GetIfBranchNodes()
        {
            return _ifBranch;
        }

        public IList<Node> GetElseBranchNodes()
        {
            return _elseBranch;
        }

        public override Shape GetShape()
        {
            return Shape.Diamond;
        }

        public override string GetNodeType()
        {
            return "if";
        }

        public override int GetNumberOfChildren()
        {
            return 3;
        }

        public void Add(Node newNode)
        {
            if (!_ifBranchDone)
            {
                _ifBranch.Add(newNode);
            }
            else
            {
                _elseBranch.Add(newNode);
            }
        }

        public void SetIfBranchToDone()
        {
            _ifBranchDone = true;
        }
    }
}