using System;
using System.Collections.Generic;
using CodeFlowAnalyzer.Nodes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeFlowAnalyzer
{
    public class SyntaxWalker : CSharpSyntaxWalker
    {
        private readonly GlobalNode _startingNode;
        private readonly Stack<Node> _scope;
        private readonly IList<Node> _allNodes;
        private int _nodeCounter;
        private int _scopeCounter;

        public SyntaxWalker()
        {
            _scope = new Stack<Node>();
            _allNodes = new List<Node>();
            
            _startingNode = new GlobalNode("START", GetNewId(), GetNewScope());
            
            _allNodes.Add(_startingNode);
            _scope.Push(_startingNode);
        }

        public void OutputFlowChartToFile(string fileBaseName = "output")
        {
            var endingNode = new StatementNode("END", GetNewId(), _startingNode.GetScope());
            
            _allNodes.Add(endingNode);
            _startingNode.Add(endingNode);
            
            var ui = new FlowChartUi(_startingNode, _allNodes);
            ui.Process();
            ui.OutputGraphToFile(fileBaseName);
        }

        public override void VisitLocalDeclarationStatement(LocalDeclarationStatementSyntax node)
        {
            VisitStatementSyntax(node);
            base.VisitLocalDeclarationStatement(node);
        }

        public override void VisitAssignmentExpression(AssignmentExpressionSyntax node)
        {
            VisitExpressionSyntax(node);
            base.VisitAssignmentExpression(node);
        }

        public override void VisitIfStatement(IfStatementSyntax node)
        {
            var currentScope = GetCurrentScopeNode();
            IfNode conditionalNode = new IfNode(node.Condition.ToString(), GetNewId(), GetNewScope());

            _allNodes.Add(conditionalNode);
            currentScope.Item2.Add(conditionalNode);
            _scope.Push(conditionalNode);
            
            base.VisitIfStatement(node);
            
            conditionalNode.SetIfBranchToDone();

            _scope.Pop();
        }

        public override void VisitElseClause(ElseClauseSyntax node)
        {
            var currentScope = GetCurrentScopeNode();

            (currentScope.Item1 as IfNode).SetIfBranchToDone();
            
            base.VisitElseClause(node);
        }

        private void VisitStatementSyntax(StatementSyntax node)
        {
            var currentScope = GetCurrentScopeNode();
            
            var newNode = new StatementNode(node.ToString(), GetNewId(), currentScope.Item1.GetScope());

            _allNodes.Add(newNode);
            currentScope.Item2.Add(newNode);
        }

        private void VisitExpressionSyntax(ExpressionSyntax node)
        {
            var currentScope = GetCurrentScopeNode();
            
            var newNode = new StatementNode(node.ToString(), GetNewId(), currentScope.Item1.GetScope());
            
            _allNodes.Add(newNode);
            currentScope.Item2.Add(newNode);
        }

        private int GetNewId()
        {
            return _nodeCounter++;
        }
        
        private int GetNewScope()
        {
            return _scopeCounter++;
        }

        private Tuple<Node, IScope> GetCurrentScopeNode()
        {
            var currentScopeNode = _scope.Peek();
            var castCurrentScopeNode = currentScopeNode as IScope;
            if (castCurrentScopeNode == null)
            {
                throw new Exception($"Reached a bad state. Expected IScope node.");
            }
            
            return new Tuple<Node, IScope>(currentScopeNode, castCurrentScopeNode);
        }
    }
}