using System;
using System.Text;
using NUnit.Framework;
using CodeFlowAnalyzer;
using CodeFlowAnalyzer.GraphString;
using CodeFlowAnalyzer.Nodes;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeFlowAnalyzer.Test.IntegrationTests
{
    public class Tests
    {
        private SyntaxWalker _syntaxWalker;
        
        [SetUp]
        public void Setup()
        {
            _syntaxWalker = new SyntaxWalker();
        }

        [Test]
        public void SingleIf_Test()
        {
            var programText = @"
            using System;
                
            namespace TopLevel
            {             
                public class Foo 
                { 
                    public Foo()
                    {
                        if (1 == 1) 
                        { 
                            var x = 1; 
                            var y = 2; 
                        } else 
                        { 
                            var a = 2; 
                            var b = 3; 
                        }
                        var r = 123;
                    }
                }                
            }";
            
            SyntaxTree tree = CSharpSyntaxTree.ParseText(programText);
            CompilationUnitSyntax root = tree.GetCompilationUnitRoot();
            
            var walker = new SyntaxWalker();
            walker.Visit(root);
            walker.OutputFlowChartToFile();
            
            var stringBuilder = new GraphStringBuilder();
            stringBuilder
                .AddNodeDeclaration(new GlobalNode("START", 0))
                .AddNodeDeclaration(new IfNode("1 == 1", 1))
                .AddNodeDeclaration(new StatementNode("var x = 1;", 2))
                .AddNodeDeclaration(new StatementNode("var y = 2;", 3))
                .AddNodeDeclaration(new StatementNode("var a = 2;", 4))
                .AddNodeDeclaration(new StatementNode("var b = 3;", 5))
                .AddNodeDeclaration(new StatementNode("var r = 123;", 6))
                .AddNodeDeclaration(new StatementNode("END", 7))
                .AddNodeLink(6, 7)
                .AddNodeLink(3, 6)
                .AddNodeLink(2, 3)
                .AddNodeLink(1, 2)
                .AddNodeLink(5, 6)
                .AddNodeLink(4, 5)
                .AddNodeLink(1, 4)
                .AddNodeLink(0, 1)
                .EndGraph();
            
            var expectedResult = stringBuilder.ToString();
            var actualResult = GetResultFromFile();

            Assert.AreEqual(expectedResult, actualResult);
        }
        
        [Test]
        public void SingleNestedIf_Test()
        {
            var programText = @"
            using System;
                
            namespace TopLevel
            {
                public class Foo 
                { 
                    public Foo1()
                    {
                        if (1 == 1) 
                        { 
                            var x = 1;
                            if (2 == 2) {
                                var z = 2;
                            }
                        }
                    }
                }
            }";
            
            SyntaxTree tree = CSharpSyntaxTree.ParseText(programText);
            CompilationUnitSyntax root = tree.GetCompilationUnitRoot();
            
            var walker = new SyntaxWalker();
            walker.Visit(root);
            walker.OutputFlowChartToFile();
            
            var stringBuilder = new GraphStringBuilder();
            stringBuilder
                .AddNodeDeclaration(new GlobalNode("START", 0))
                .AddNodeDeclaration(new IfNode("1 == 1", 1))
                .AddNodeDeclaration(new StatementNode("var x = 1;", 2))
                .AddNodeDeclaration(new IfNode("2 == 2;", 3))
                .AddNodeDeclaration(new StatementNode("var z = 2;", 4))
                .AddNodeDeclaration(new StatementNode("END", 5))
                .AddNodeLink(4, 5)
                .AddNodeLink(3, 4)
                .AddNodeLink(3, 5)
                .AddNodeLink(2, 3)
                .AddNodeLink(1, 2)
                .AddNodeLink(1, 5)
                .AddNodeLink(0, 1)
                .EndGraph();
            var expectedResult = stringBuilder.ToString();
            var actualResult = GetResultFromFile();

            Assert.AreEqual(expectedResult, actualResult);
        }
        
        [Test]
        public void SingleNestedIfElse_Test()
        {
            var programText = @"
            using System;
                
            namespace TopLevel
            {                         
                public class Foo 
                {
                    public void Foo1()
                    {
                        if (1 == 1) 
                        { 
                            var x = 1;
                            var y = 2;
                            if (2 == 2) {
                                var z = 0;
                            } else {
                                var a = 3;
                            }
                        }
                        var r = 123;
                    }
                }                
            }";
            
            SyntaxTree tree = CSharpSyntaxTree.ParseText(programText);
            CompilationUnitSyntax root = tree.GetCompilationUnitRoot();
            
            var walker = new SyntaxWalker();
            walker.Visit(root);
            walker.OutputFlowChartToFile();
            
            var stringBuilder = new GraphStringBuilder();
            stringBuilder
                .AddNodeDeclaration(new GlobalNode("START", 0))
                .AddNodeDeclaration(new IfNode("1 == 1", 1))
                .AddNodeDeclaration(new StatementNode("var x = 1;", 2))
                .AddNodeDeclaration(new StatementNode("var y = 2;", 3))
                .AddNodeDeclaration(new IfNode("2 == 2", 4))
                .AddNodeDeclaration(new StatementNode("var z = 0;", 5))
                .AddNodeDeclaration(new StatementNode("var a = 3;", 6))
                .AddNodeDeclaration(new StatementNode("var r = 123;", 7))
                .AddNodeDeclaration(new StatementNode("END", 8))
                .AddNodeLink(7, 8)
                .AddNodeLink(5, 7)
                .AddNodeLink(4, 5)
                .AddNodeLink(6, 7)
                .AddNodeLink(4, 6)
                .AddNodeLink(3, 4)
                .AddNodeLink(2, 3)
                .AddNodeLink(1, 2)
                .AddNodeLink(1, 7)
                .AddNodeLink(0, 1)
                .EndGraph();
            
            var expectedResult = stringBuilder.ToString();
            var actualResult = GetResultFromFile();

            Assert.AreEqual(expectedResult, actualResult);
        }
        
        [Test]
        public void DoubleIfElse_Test()
        {
            var programText = @"
            using System;
                
            namespace TopLevel
            {                         
                public class Foo 
                {
                    public void Foo1()
                    {
                        if (1 == 1) 
                        { 
                            var x = 1;
                        }
                        else
                        {
                            var y = 2;
                        }

                        if (2 == 2) 
                        { 
                            var a = 1;
                        }
                        else
                        {
                            var b = 2;
                        }

                        var r = 123;
                    }
                }                
            }";
            
            SyntaxTree tree = CSharpSyntaxTree.ParseText(programText);
            CompilationUnitSyntax root = tree.GetCompilationUnitRoot();
            
            var walker = new SyntaxWalker();
            walker.Visit(root);
            walker.OutputFlowChartToFile();
            
            var stringBuilder = new GraphStringBuilder();
            stringBuilder
                .AddNodeDeclaration(new GlobalNode("START", 0))
                .AddNodeDeclaration(new IfNode("1 == 1", 1))
                .AddNodeDeclaration(new StatementNode("var x = 1;", 2))
                .AddNodeDeclaration(new StatementNode("var y = 2;", 3))
                .AddNodeDeclaration(new IfNode("2 == 2", 4))
                .AddNodeDeclaration(new StatementNode("var a = 1;", 5))
                .AddNodeDeclaration(new StatementNode("var b = 2;", 6))
                .AddNodeDeclaration(new IfNode("var r = 123;", 7))
                .AddNodeDeclaration(new StatementNode("END", 8))
                .AddNodeLink(7, 8)
                .AddNodeLink(5, 7)
                .AddNodeLink(4, 5)
                .AddNodeLink(6, 7)
                .AddNodeLink(4, 6)
                .AddNodeLink(2, 4)
                .AddNodeLink(1, 2)
                .AddNodeLink(3, 4)
                .AddNodeLink(1, 3)
                .AddNodeLink(0, 1)
                .EndGraph();
            
            var expectedResult = stringBuilder.ToString();
            var actualResult = GetResultFromFile();

            Assert.AreEqual(expectedResult, actualResult);
        }
        
        [Test]
        public void DoubleNestedIfElse_Test()
        {
            var programText = @"
            using System;
                
            namespace TopLevel
            {                         
                public class Foo 
                {
                    public void Foo1()
                    {
                        if (1 == 1) 
                        { 
                            if (3 == 3)
                            {
                                var x = 1;
                            }
                            else
                            {
                                var c = 100;
                            }
                        }
                        else
                        {
                            if (4 == 4)
                            {
                                var d = 200;
                            }
                            else
                            {
                                var e = 400;
                            }
                        }

                        if (5 == 5) 
                        { 
                            if (6 == 6)
                            {
                                var f = 1;
                            }
                            else
                            {
                                var g = 1100;
                            }
                        }
                        else
                        {
                            if (7 == 7)
                            {
                                var h = 30;
                            }
                            else
                            {
                                var i = 700;
                            }
                        }

                        var r = 123;
                    }
                }                
            }";
            
            SyntaxTree tree = CSharpSyntaxTree.ParseText(programText);
            CompilationUnitSyntax root = tree.GetCompilationUnitRoot();
            
            var walker = new SyntaxWalker();
            walker.Visit(root);
            walker.OutputFlowChartToFile();
            
            var stringBuilder = new GraphStringBuilder();
            stringBuilder
                .AddNodeDeclaration(new GlobalNode("START", 0))
                .AddNodeDeclaration(new IfNode("1 == 1", 1))
                .AddNodeDeclaration(new IfNode("3 == 3", 2))
                .AddNodeDeclaration(new StatementNode("var x = 1;", 3))
                .AddNodeDeclaration(new StatementNode("var c = 100;", 4))
                .AddNodeDeclaration(new IfNode("4 == 4", 5))
                .AddNodeDeclaration(new StatementNode("var d = 200;", 6))
                .AddNodeDeclaration(new StatementNode("var e = 400;", 7))
                .AddNodeDeclaration(new IfNode("5 == 5", 8))
                .AddNodeDeclaration(new IfNode("6 == 6", 9))
                .AddNodeDeclaration(new StatementNode("var f = 1;", 10))
                .AddNodeDeclaration(new StatementNode("var g = 1100;", 11))
                .AddNodeDeclaration(new IfNode("7 == 7", 12))
                .AddNodeDeclaration(new StatementNode("var h = 30;", 13))
                .AddNodeDeclaration(new StatementNode("var i = 700;", 14))
                .AddNodeDeclaration(new StatementNode("var r = 123;", 15))
                .AddNodeDeclaration(new StatementNode("END", 16))
                .AddNodeLink(15, 16)
                .AddNodeLink(10, 15)
                .AddNodeLink(9, 10)
                .AddNodeLink(11, 15)
                .AddNodeLink(9, 11)
                .AddNodeLink(8, 9)
                .AddNodeLink(13, 15)
                .AddNodeLink(12, 13)
                .AddNodeLink(14, 15)
                .AddNodeLink(12, 14)
                .AddNodeLink(8, 12)
                .AddNodeLink(3, 8)
                .AddNodeLink(2, 3)
                .AddNodeLink(4, 8)
                .AddNodeLink(2, 4)
                .AddNodeLink(1, 2)
                .AddNodeLink(6, 8)
                .AddNodeLink(5, 6)
                .AddNodeLink(7, 8)
                .AddNodeLink(5, 7)
                .AddNodeLink(1, 5)
                .AddNodeLink(0, 1)
                .EndGraph();
            
            var expectedResult = stringBuilder.ToString();
            var actualResult = GetResultFromFile();

            Assert.AreEqual(expectedResult, actualResult);
        }

        private string GetResultFromFile()
        {
            var filePath = AppDomain.CurrentDomain.BaseDirectory + "output.dot";
            string text = System.IO.File.ReadAllText(filePath);
            return text;
        }
    }
}